$(function(){
    
    //fade In
	$('head').append(
		'<style type="text/css">#container {display:none;}'
	);
	$(window).load(function() {
		$('#container').delay(600).fadeIn("1500");	
	});

	//高さ取得
	var slide = $(".slide")
	var sh	= slide.height()
	var wh  = $(window).height()

	//ボタンクリック時画面遷移

    $(".down-btn").click(function(){
        $('.mordal-content').fadeOut('slow');
            $('.modal-orverlay').fadeOut('slow',function(){
                // オーバーレイを削除
                $('.modal-orverlay').remove();
                $(window).off('.noScroll');
            });
         $("body").animate({
			scrollTop: $("#slide-profile").offset().top
    }, 600);
        return false;
    });	
    
    
     $(".t-link").click(function(){
         $('.mordal-content').fadeOut('slow');
            $('.modal-orverlay').fadeOut('slow',function(){
                // オーバーレイを削除
                $('.modal-orverlay').remove();
                $(window).off('.noScroll');
            });
         
         $("body").animate({
			scrollTop: $("#slide-top").offset().top
    }, 600); 
        return false;
    });	
    
    
     $(".p-link").click(function(){
         $('.mordal-content').fadeOut('slow');
            $('.modal-orverlay').fadeOut('slow',function(){
                // オーバーレイを削除
                $('.modal-orverlay').remove();
                $(window).off('.noScroll');
            });
         
         $("body").animate({
			scrollTop: $("#slide-profile").offset().top
    }, 600); 
        return false;
    });	
    
    
     $(".s-link").click(function(){
         $('.mordal-content').fadeOut('slow');
            $('.modal-orverlay').fadeOut('slow',function(){
                // オーバーレイを削除
                $('.modal-orverlay').remove();
                $(window).off('.noScroll');
            });
         
         $("body").animate({
			scrollTop: $("#slide-skills").offset().top
    }, 600); 
        return false;
    });	
     $(".w-link").click(function(){
         $('.mordal-content').fadeOut('slow');
            $('.modal-orverlay').fadeOut('slow',function(){
                // オーバーレイを削除
                $('.modal-orverlay').remove();
                $(window).off('.noScroll');
            });
         
         $("body").animate({
			scrollTop: $("#slide-works").offset().top
    }, 600); 
        return false;
    });	


    //モーダルウィンドウ設定
    $('.menu-trigger').click(function(){
        $('body').append('<div class="modal-orverlay"></div>');
    	$('.modal-orverlay').fadeIn();
        
    	var modal = $('.mordal-content')
        modalResize();
        $(modal).fadeIn('slow');
        
         $(window).on('touchmove.noScroll', function(e) {
            e.preventDefault();
        });
   
		// 「.modal-overlay」か「.modal-close」をクリック
        $('.modal-orverlay, .close-btn, li').off().click(function(){
            $(modal).fadeOut('slow');
            $('.modal-orverlay').fadeOut('slow',function(){
                // オーバーレイを削除
                $('.modal-orverlay').remove();
                $(window).off('.noScroll');
            });
        });

        $(window).on('resize', function(){
            modalResize();
        });

        function modalResize(){
            var w = $(window).width();
            var h = $(window).height();

            var x = (w - $(modal).outerWidth(true)) / 2;
            var y = (h - $(modal).outerHeight(true)) / 2;

            $(modal).css({'left': x + 'px','top': y + 'px'});

    
        };
    });

    $(".close-btn").click(function(){
    	$(".modal-orverlay").fadeOut();
    	$(window).off('.noScroll');
    });


    // var linkName = [".down-btn", ".t-link", ".p-link", ".s-link", ".w-link"];
    // var linkSlide　= ["#slide-profile", "#slide-top", "#slide-skills", "#slide-profile", "#slide-works"];

 //   console.log("リンクの名前：" + linkName[1]);
 //   console.log("スライドの名前" + $(linkSlide[3]).offset().top);
 //    for(i=0; i < 5; i++){
 //    	$(linkName[i]).click(function(){
 //         $("body").animate({
	// 		scrollTop: $(linkSlide[i]).offset().top
 //    }, 600);
 //        return false;
 //    });
	// }
	

	// //スクロール画面遷移
	// var slide1 = $("#slide-top").offset().top;
	// var slide2 = $("#slide-profile").offset().top;
	// var slide3 = $("#slide-works").offset().top;


	// var before = $(window).scrollTop();
	// $(".slide").scroll(function() {
	// 	var targetY = $(this).offset().top
	// $("html,body").animate({scrollTop:targetY});
	// var after = $(window).scrollTop();
	// if(before > after) {
	// console.log("上です。")
	
	// $("body").animate({
			
 //    }, 1000);
	// }
	// else if(before < after) {
	// console.log("下です。")
	// }
	// before = after;
	// });


	// $(".slide").scroll(function(){
 //            $("body").animate({
	// 		scrollTop: $(this).offset().top
 //    }, 1000);
        
 //    });	


	//スライドショー設定

	// var href,
	// 	startX,
	// 	startY,
	// 	moveX,
	// 	moveY;

  
 //  $("#slide").css("margin-left","-320px");
 //  $("#slide a").click(function(){
 //    return false;
 //  }).on("touchstart",function(){
	// 	href = $(this).attr("href")
	// })

	// $("#slide").on("touchstart",function(){
	// 	console.log("touchstart");
	// 	event.preventDefault();

	// 	startX = event.touches[0].pageX;
	// 	startY = event.touches[0].pageY;
	// 	moveX = 0;
	// 	moveY = 0;
	// }).on("touchmove",function(){
	// 	console.log("touchmove");
	// 	moveX = event.touches[0].pageX-startX;
	// 	moveY = event.touches[0].pageY-startY;
	// 	$(this).css("margin-left",moveX-320);
	// }).on("touchend",function(){
	// 	console.log("touchend");

	// 	if(moveX>10){
	// 		console.log("左スワイプ");
	// 		$(this).animate({
	// 			"margin-left":0
	// 		},function(){
	// 			$(this).css("margin-left","-320px");
	// 			$("#slide li:last-child").prependTo("#slide");
	// 		});

	// 	}else if(moveX<-10){
	// 		console.log("右スワイプ");
	// 		$(this).animate({
	// 			"margin-left":-640
	// 		},function(){
	// 			$(this).css("margin-left","-320px");
	// 			$("#slide li:first-child").appendTo("#slide");
	// 		});

	// 	}else if(moveY<10 && moveY>-10){
	// 		console.log("タップ");
	// 		location.href = href;

	// 	}else{
	// 		console.log("スクロール");
	// 		$(this).animate({
	// 			"margin-left":-320
	// 		});
	// 	}
	// });

		var divName = ["#profile-card", "#works-card"];
        var liName  = ["#profile-card li","#works-card li"]
		var slideName = [0, 0];
		var slideNum;
		var slideWidth;


	$(window).resize(function(){

        
		for(var i=0; i < 2; i++){
			console.log(divName[i]);
			console.log(slideName[i]);

			var currentDiv = divName[i];
			console.log(currentDiv);

			//横スライドショー設定　プロフィール
			
			slideWidth = $("body").outerWidth(true); // slide-wrapperの幅を取得して代入
			slideNum = $(currentDiv + " li").length;
			console.log("スライドの幅" + slideWidth);
			console.log("スライドの数" + slideNum)
			
            $(liName[i]).css('width',$(window).width());
			var slideSetWidth = slideWidth * slideNum; // .slideの幅×数で求めた値を代入
			$(divName[i]).css('width', slideSetWidth); // .slideSetのスタイルシートにwidth: slideSetWidthを指定
			console.log("幅×数" + slideSetWidth);

		}

	}).resize();
	 
		
		// アニメーションを実行する独自関数
		var sliding = function(){

			var slideCurrent		
			 // slideCurrentが0以下だったら
		  if( slideName[0] < 0 ){
		    slideName[0] = slideNum - 1;
		 
		  // slideCurrentがslideNumを超えたら
		  }else if( slideName[0] > slideNum - 1){ 
		    slideName[0] = 0;
		 
		  }
		  $('#profile-card').stop().animate({
		    left:　slideName[0] * -slideWidth
		  });

		    console.log("スライドの幅" + slideWidth)
		}

		// 前へボタンが押されたとき
		$('#slide-profile .prev').click(function(){
		  slideName[0]--;
		  sliding();
		});

		// 次へボタンが押されたとき
		$('#slide-profile .next').click(function(){
		  slideName[0]++;
		  sliding();
		});


		
	 
		// アニメーションを実行する独自関数
		var wsliding = function(){

			 // slideCurrentが0以下だったら
		  if( slideName[1] < 0 ){
		    slideName[1] = slideNum - 1;
		 
		  // slideCurrentがslideNumを超えたら
		  }else if( slideName[1] > slideNum - 1){ 
		    slideName[1] = 0;
		 
		  }

		  $('#works-card').stop().animate({
		    left: slideName[1] * -slideWidth
		  });
		}

		// 前へボタンが押されたとき
		$('#slide-works .prev').click(function(){
		  slideName[1]--;
		  wsliding();
		});

		// 次へボタンが押されたとき
		$('#slide-works .next').click(function(){
		  slideName[1]++;
		  wsliding();
		});


	// var href,
	// 	startX,
	// 	startY,
	// 	moveX,
	// 	moveY;

  

 //  $(divName[i]).click(function(){
 //    return false;
 //  }).on("touchstart",function(){
	// 	href = $(this).attr("href")
	// })

	// $(divName[i]).on("touchstart",function(){
	// 	console.log("touchstart");
	// 	event.preventDefault();

	// 	startX = event.touches[0].pageX;
	// 	startY = event.touches[0].pageY;
	// 	moveX = 0;
	// 	moveY = 0;
	// }).on("touchmove",function(){
	// 	console.log("touchmove");
	// 	moveX = event.touches[0].pageX-startX;
	// 	moveY = event.touches[0].pageY-startY;
	// 	$(this).css("margin-left",moveX-320);
	// }).on("touchend",function(){
	// 	console.log("touchend");

	// 	if(moveX>10){
	// 		console.log("左スワイプ");
	// 		$(this).animate({
				
	// 		},function(){
	// 			$(this).css("margin-left","-320px");
	// 		});

	// 	}else if(moveX<-10){
	// 		console.log("右スワイプ");
	// 		$(this).animate({
				
	// 		},function(){
	// 			$(this).css("margin-left","-320px");
	// 		});

	// 	}else if(moveY<10 && moveY>-10){
	// 		console.log("タップ");
	// 		location.href = href;

	// 	}else{
	// 		console.log("スクロール");
	// 		$(this).animate({
	// 			"margin-left":-320
	// 		});
	// 	}
	// });


	
});